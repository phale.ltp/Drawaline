#include "Ellipse.h"
#include <iostream>

void Draw4Points(int xc, int yc, int x, int y, SDL_Renderer *ren)
{
	SDL_RenderDrawPoint(ren, xc + x, yc + y);
	SDL_RenderDrawPoint(ren, xc + x, yc - y);
	SDL_RenderDrawPoint(ren, xc - x, yc + y);
	SDL_RenderDrawPoint(ren, xc - x, yc - y);
}

void BresenhamDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
	// Area 1
	int x, y, x2, a2, b2;
	float p;
	a2 = a * a;
	b2 = b * b;
	x = 0;
	y = b;
	p = -2 * a2*b + a2 + 2 * b2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) < (a2*a2))
	{
	
		if (p <= 0)
		{
			p += 4 * b2*x + 6 * b2;
		}
		else
		{
			y--;
			p += 4 * b2*x - 4 * a2*y;
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	x = a;
	y =  0;
	p = -2 * a*b2 + b2 + 2 * a2;
	Draw4Points(xc, yc, x, y, ren);
	while (x*x*(a2 + b2) >= (a2*a2))
	{
		
		if (p <= 0)
		{
			p += 4 * a2*y + 6 * a2;
		}
		else
		{
			x--;
			p += 4 * a2*y - 4 * b2*x;
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}
}

void MidPointDrawEllipse(int xc, int yc, int a, int b, SDL_Renderer *ren)
{
//     //Area 1
	float p, a2, b2;
	int x, x2, y, y2;
	a2 = a * a;
	b2 = b * b;
	x = 0;
	y = b;
	
	p = b2 - a2 * b + a2 / float(4);
	Draw4Points(xc, yc, x, y, ren);
	while (x * x*(a2 + b2) < (a2*a2))
	{
		if (p <= 0)
		{
			p += 2*b2 * x + 3 * b2;
		}
		else
		{
			y--;
			p += 2 * b2*x - 2 * a2*y + 2 * a2 + 3 * b2;
			
		}
		x++;
		Draw4Points(xc, yc, x, y, ren);
	}
    // Area 2
	x = a;
	y = 0;
	p = a2 - b2 * a + b2 / float(4);
	Draw4Points(xc, yc, x, y, ren);
	while (x * x*(a2 + b2) >= (a2*a2))
	{
		if (p <= 0)
		{
			p += 2*a2 * y + 3 * a2;
		}
		else
		{
			x--;
			p += 2 * a2*y - 2 * b2*x + 2 * b2 + 3 * a2;
			
		}
		y++;
		Draw4Points(xc, yc, x, y, ren);
	}

}